!******************************************************************************
! This module defines the PEST object used in the vmec2pest calculation
! Author: B.J. Faber (bfaber@wisc.edu)
! Creation date: 30.08.2018
!******************************************************************************

module pest_object
  use types, only: dp, pi
  use vmec_object, only: VMEC_Obj, create_VMEC_Obj, destroy_VMEC_Obj
  implicit none

  public PEST_Obj, create_PEST_Obj, destroy_PEST_Obj, set_PEST_reference_values, get_PEST_data

  private

  type :: PEST_Obj
    ! Pointer to the VMEC equilibrium
    type(VMEC_Obj) :: vmec

    ! The rotational transform iota
    real(dp), dimension(:), allocatable :: iota

    ! Safety factor q = 1/iota
    real(dp), dimension(:), allocatable :: safety_factor_q

    ! Magnetic shear s_hat = (x/q) * (d q / d x) where x = Aminor_p * sqrt(psi_toroidal / psi_{toroidal,edge})
    ! and Aminor_p is the minor radius calculated by VMEC.
    real(dp), dimension(:), allocatable :: s_hat

    ! L_ref is the reference length used for gs2's normalization, in meters.
    real(dp) :: L_ref

    ! The major radius in meters 
    real(dp) :: major_R

    ! The minor radius in meters
    real(dp) :: minor_r
  
    ! B_ref is the reference magnetic field strength used for gs2's normalization, in Tesla.
    real(dp) :: B_ref

    ! Integers representing the number of points in each SFL direction 
    integer :: n_psi, n_alpha, n_zeta

    ! Integers representing the starting and ending array indices
    integer :: idx_psi_1, idx_psi_2, idx_alpha_1, idx_alpha_2, idx_zeta_1, idx_zeta_2

    ! On exit, s holds the grid points in the first coordinate (flux surface label)
    ! in units of normalized flux
    ! Typically, this is x1 = s = psi_toroidal/psi_edge 
    real(dp), dimension(:,:,:), allocatable :: psi

    ! On exit, x2 holds the grid points in the second coordinate (field line label)
    ! Typically, this is x2 = alpha = theta_p - iota * zeta, where zeta is the PEST (geometric) toroidal angle
    ! and theta_p is the PEST poloidal angle
    real(dp), dimension(:,:,:), allocatable :: alpha

    ! On exit, x3 holds the grid points in the third coordinate (field line following coordinate)
    ! Typically, this is x3 = zeta, where zeta is the PEST (geometric) toroidal angle
    real(dp), dimension(:,:,:), allocatable :: zeta

    ! VMEC supplies the normalized toroidal flux, thus it is natural to use the
    ! toroidal zeta coordinate as the angle parameterizing the field line.
    ! However some codes use the poloidal angle theta, related to zeta by theta =
    ! iota*zeta.  The maximum extent of the flux tube and the coordinate used in
    ! output can be chosen to be either theta or zeta, but the calculation itself
    ! is performed using the zeta coordinate

    real(dp) :: zeta_max_interval

    ! Arrays that contain the geometric elements on the surface
    real(dp), dimension(:,:,:), allocatable :: bmag  ! Magnitude of B (|B|)
    real(dp), dimension(:,:,:), allocatable :: jacobian ! Jacobian (grad psi . grad alpha x grad zeta)^(-1)
    real(dp), dimension(:,:,:), allocatable :: grad_psi_X ! X Cartesian component of gradient of psi
    real(dp), dimension(:,:,:), allocatable :: grad_psi_Y ! Y Cartesian component of gradient of psi
    real(dp), dimension(:,:,:), allocatable :: grad_psi_Z ! Z Cartesian component of gradient of psi
    real(dp), dimension(:,:,:), allocatable :: grad_alpha_X ! X Cartesian component of gradient of alpha
    real(dp), dimension(:,:,:), allocatable :: grad_alpha_Y ! Y Cartesian component of gradient of alpha
    real(dp), dimension(:,:,:), allocatable :: grad_alpha_Z ! Z Cartesian component of gradient of alpha
    real(dp), dimension(:,:,:), allocatable :: grad_zeta_X ! X Cartesian component of gradient of zeta
    real(dp), dimension(:,:,:), allocatable :: grad_zeta_Y ! Y Cartesian component of gradient of zeta
    real(dp), dimension(:,:,:), allocatable :: grad_zeta_Z ! Z Cartesian component of gradient of zeta
    real(dp), dimension(:,:,:), allocatable :: grad_theta_X ! X Cartesian component of gradient of theta
    real(dp), dimension(:,:,:), allocatable :: grad_theta_Y ! Y Cartesian component of gradient of theta
    real(dp), dimension(:,:,:), allocatable :: grad_theta_Z ! Z Cartesian component of gradient of theta
    real(dp), dimension(:,:,:), allocatable :: basis_psi_X ! X Cartesian component of covariant basis of psi
    real(dp), dimension(:,:,:), allocatable :: basis_psi_Y ! Y Cartesian component of covariant basis of psi
    real(dp), dimension(:,:,:), allocatable :: basis_psi_Z ! Z Cartesian component of covariant basis of psi
    real(dp), dimension(:,:,:), allocatable :: basis_alpha_X ! X Cartesian component of covariant basis of alpha
    real(dp), dimension(:,:,:), allocatable :: basis_alpha_Y ! Y Cartesian component of covariant basis of alpha
    real(dp), dimension(:,:,:), allocatable :: basis_alpha_Z ! Z Cartesian component of covariant basis of alpha
    real(dp), dimension(:,:,:), allocatable :: basis_zeta_X ! X Cartesian component of covariant basis of zeta
    real(dp), dimension(:,:,:), allocatable :: basis_zeta_Y ! Y Cartesian component of covariant basis of zeta
    real(dp), dimension(:,:,:), allocatable :: basis_zeta_Z ! Z Cartesian component of covariant basis of zeta
    real(dp), dimension(:,:,:), allocatable :: basis_theta_X ! X Cartesian component of covariant basis of theta
    real(dp), dimension(:,:,:), allocatable :: basis_theta_Y ! Y Cartesian component of covariant basis of theta
    real(dp), dimension(:,:,:), allocatable :: basis_theta_Z ! Z Cartesian component of covariant basis of theta
    real(dp), dimension(:,:,:), allocatable :: B_X ! Cartesian X component of vector B
    real(dp), dimension(:,:,:), allocatable :: B_Y ! Cartesian Y component of vector B
    real(dp), dimension(:,:,:), allocatable :: B_Z ! Cartesian Z component of vector B
    real(dp), dimension(:,:,:), allocatable :: grad_B_X ! Cartesian X component of grad |B|
    real(dp), dimension(:,:,:), allocatable :: grad_B_Y ! Cartesian Y component of grad |B|
    real(dp), dimension(:,:,:), allocatable :: grad_B_Z ! Cartesian Z component of grad |B|
    real(dp), dimension(:,:,:), allocatable :: g_psi_psi ! Metric element gss
    real(dp), dimension(:,:,:), allocatable :: g_psi_alpha ! Metric element gsa
    real(dp), dimension(:,:,:), allocatable :: g_alpha_alpha ! Metric element gaa
    real(dp), dimension(:,:,:), allocatable :: g_psi_zeta ! Metric element gsz
    real(dp), dimension(:,:,:), allocatable :: g_alpha_zeta ! Metric element gaz
    real(dp), dimension(:,:,:), allocatable :: g_zeta_zeta ! Metric element gzz
    real(dp), dimension(:,:,:), allocatable :: grad_B_drift_psi !  Psi component of grad B drift
    real(dp), dimension(:,:,:), allocatable :: grad_B_drift_alpha ! Alpha component of grad B drift
    real(dp), dimension(:,:,:), allocatable :: normal_curv ! Normal curvature 
    real(dp), dimension(:,:,:), allocatable :: geodesic_curv ! Geodesic curvature
    real(dp), dimension(:,:,:), allocatable :: d_B_d_zeta ! Derivative of |B| w.r.t. the zeta coordinate

    real(dp), dimension(:,:,:), allocatable :: Rsurf ! The R coordinate of the surfaces
    real(dp), dimension(:,:,:), allocatable :: Zsurf ! The Z coordinate of the surfaces
    real(dp), dimension(:,:,:), allocatable :: theta_pest ! theta = theta_vmec + Lambda
    real(dp), dimension(:,:,:), allocatable :: d_Lambda_d_theta_vmec ! Needed for Jacobian calculation

  end type ! end type PEST_Obj


  interface create_PEST_Obj
    module procedure create_from_VMEC
    module procedure create_from_field_line_file
  end interface

  interface get_PEST_data
    module procedure get_PEST_VMEC_data
    module procedure get_PEST_radial_data
    module procedure get_PEST_field_line_data
    module procedure get_PEST_surface_data
    module procedure get_PEST_volume_data
  end interface

contains

  type(PEST_Obj) function create_from_VMEC(VMEC_id,surfaces,n_surf,n_field_lines,n_parallel_pts) result(pest)
    character(len=:), intent(in), pointer :: VMEC_id
    real(dp), dimension(:), intent(in) :: surfaces
    integer, intent(in) :: n_surf, n_field_lines, n_parallel_pts
    logical :: verbose = .false.
    integer :: j,k,n
    real(dp) :: surf

    type(VMEC_Obj) :: vmec
    vmec = create_VMEC_Obj(VMEC_id)

!    j = 0
!    do while (surfaces(j+1) .gt. 1e-8)
!      j = j+1
!      print *,surfaces(j), j
!    end do
!
    !if (j .eq. 0) then
    !  print *, "VMEC2PEST error! Cannot compute a surface at s = 0"
    !  stop
    !end if 
    !nsurf = j

    pest%vmec = vmec
    pest%n_psi = n_surf
    pest%n_alpha = n_field_lines
    pest%n_zeta = n_parallel_pts+1

    pest%idx_psi_1 = 0
    pest%idx_psi_2 = n_surf-1
    pest%idx_alpha_1 = 0
    pest%idx_alpha_2 = n_field_lines-1
    pest%idx_zeta_1 = -n_parallel_pts/2
    pest%idx_zeta_2 = n_parallel_pts/2

    if(allocated(pest%iota)) deallocate(pest%iota)
    if(allocated(pest%s_hat)) deallocate(pest%s_hat)
    if(allocated(pest%safety_factor_q)) deallocate(pest%safety_factor_q)

    if(allocated(pest%psi)) deallocate(pest%psi)
    if(allocated(pest%alpha)) deallocate(pest%alpha)
    if(allocated(pest%zeta)) deallocate(pest%zeta)
    if(allocated(pest%grad_psi_X)) deallocate(pest%grad_psi_X)
    if(allocated(pest%grad_psi_Y)) deallocate(pest%grad_psi_Y)
    if(allocated(pest%grad_psi_Z)) deallocate(pest%grad_psi_Z)
    if(allocated(pest%grad_alpha_X)) deallocate(pest%grad_alpha_X)
    if(allocated(pest%grad_alpha_Y)) deallocate(pest%grad_alpha_Y)
    if(allocated(pest%grad_alpha_Z)) deallocate(pest%grad_alpha_Z)
    if(allocated(pest%grad_zeta_X)) deallocate(pest%grad_zeta_X)
    if(allocated(pest%grad_zeta_Y)) deallocate(pest%grad_zeta_Y)
    if(allocated(pest%grad_zeta_Z)) deallocate(pest%grad_zeta_Z)
    if(allocated(pest%grad_theta_X)) deallocate(pest%grad_theta_X)
    if(allocated(pest%grad_theta_Y)) deallocate(pest%grad_theta_Y)
    if(allocated(pest%grad_theta_Z)) deallocate(pest%grad_theta_Z)
    if(allocated(pest%basis_psi_X)) deallocate(pest%basis_psi_X)
    if(allocated(pest%basis_psi_Y)) deallocate(pest%basis_psi_Y)
    if(allocated(pest%basis_psi_Z)) deallocate(pest%basis_psi_Z)
    if(allocated(pest%basis_alpha_X)) deallocate(pest%basis_alpha_X)
    if(allocated(pest%basis_alpha_Y)) deallocate(pest%basis_alpha_Y)
    if(allocated(pest%basis_alpha_Z)) deallocate(pest%basis_alpha_Z)
    if(allocated(pest%basis_zeta_X)) deallocate(pest%basis_zeta_X)
    if(allocated(pest%basis_zeta_Y)) deallocate(pest%basis_zeta_Y)
    if(allocated(pest%basis_zeta_Z)) deallocate(pest%basis_zeta_Z)
    if(allocated(pest%basis_theta_X)) deallocate(pest%basis_theta_X)
    if(allocated(pest%basis_theta_Y)) deallocate(pest%basis_theta_Y)
    if(allocated(pest%basis_theta_Z)) deallocate(pest%basis_theta_Z)
    if(allocated(pest%B_X)) deallocate(pest%B_X)
    if(allocated(pest%B_Y)) deallocate(pest%B_Y)
    if(allocated(pest%B_Z)) deallocate(pest%B_Z)
    if(allocated(pest%grad_B_X)) deallocate(pest%grad_B_X)
    if(allocated(pest%grad_B_Y)) deallocate(pest%grad_B_Y)
    if(allocated(pest%grad_B_Z)) deallocate(pest%grad_B_Z)
    if(allocated(pest%bmag)) deallocate(pest%bmag)
    if(allocated(pest%jacobian)) deallocate(pest%jacobian)
    if(allocated(pest%g_psi_psi)) deallocate(pest%g_psi_psi)
    if(allocated(pest%g_psi_alpha)) deallocate(pest%g_psi_alpha)
    if(allocated(pest%g_alpha_alpha)) deallocate(pest%g_alpha_alpha)
    if(allocated(pest%g_psi_zeta)) deallocate(pest%g_psi_zeta)
    if(allocated(pest%g_alpha_zeta)) deallocate(pest%g_alpha_zeta)
    if(allocated(pest%g_zeta_zeta)) deallocate(pest%g_zeta_zeta)
    if(allocated(pest%grad_B_drift_psi)) deallocate(pest%normal_curv)
    if(allocated(pest%grad_B_drift_alpha)) deallocate(pest%geodesic_curv)
    if(allocated(pest%normal_curv)) deallocate(pest%normal_curv)
    if(allocated(pest%geodesic_curv)) deallocate(pest%geodesic_curv)
    if(allocated(pest%d_B_d_zeta)) deallocate(pest%d_B_d_zeta)
    if(allocated(pest%Rsurf)) deallocate(pest%Rsurf)
    if(allocated(pest%Zsurf)) deallocate(pest%Zsurf)
    if(allocated(pest%theta_pest)) deallocate(pest%theta_pest)
    if(allocated(pest%d_Lambda_d_theta_vmec)) deallocate(pest%d_Lambda_d_theta_vmec)

    allocate(pest%iota(pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%s_hat(pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%safety_factor_q(pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%psi(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%alpha(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%zeta(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%grad_psi_X(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%grad_psi_Y(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%grad_psi_Z(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%grad_alpha_X(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%grad_alpha_Y(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%grad_alpha_Z(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%grad_zeta_X(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%grad_zeta_Y(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%grad_zeta_Z(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%grad_theta_X(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%grad_theta_Y(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%grad_theta_Z(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%basis_psi_X(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%basis_psi_Y(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%basis_psi_Z(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%basis_alpha_X(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%basis_alpha_Y(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%basis_alpha_Z(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%basis_zeta_X(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%basis_zeta_Y(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%basis_zeta_Z(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%basis_theta_X(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%basis_theta_Y(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%basis_theta_Z(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%B_X(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%B_Y(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%B_Z(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%grad_B_X(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%grad_B_Y(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%grad_B_Z(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%bmag(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%jacobian(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%g_psi_psi(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%g_psi_alpha(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%g_alpha_alpha(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%g_psi_zeta(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%g_alpha_zeta(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%g_zeta_zeta(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%normal_curv(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%geodesic_curv(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%grad_B_drift_psi(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%grad_B_drift_alpha(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%d_B_d_zeta(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%Rsurf(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%Zsurf(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%theta_pest(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))
    allocate(pest%d_Lambda_d_theta_vmec(pest%idx_zeta_1:pest%idx_zeta_2, pest%idx_alpha_1:pest%idx_alpha_2, pest%idx_psi_1:pest%idx_psi_2))

    if (verbose) print *, pest%idx_psi_1, pest%idx_psi_2, pest%n_psi, surfaces(1:n_surf)
    if (verbose) print *, pest%idx_alpha_1, pest%idx_alpha_2, pest%n_alpha
    if (verbose) print *, pest%idx_zeta_1, pest%idx_zeta_2, pest%n_zeta
    do n = pest%idx_psi_1,pest%idx_psi_2
      surf = surfaces(n+pest%idx_psi_1+1)
      do k = pest%idx_alpha_1,pest%idx_alpha_2
        do j = pest%idx_zeta_1,pest%idx_zeta_2
          pest%psi(j,k,n) = surf 
        end do
      end do
    end do
  end function

  type(PEST_Obj) function create_from_field_line_file(fl_file) result(pest)
    character(len=2000), intent(in) :: fl_file

  end function

  subroutine set_PEST_reference_values(pest,norm_type)
    type(PEST_Obj), intent(inout) :: pest
    character(len=7), intent(in) :: norm_type
    real(dp) :: edge_toroidal_flux_over_2pi
    logical :: verbose
    verbose = .false.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! Set reference values
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
    edge_toroidal_flux_over_2pi = pest%vmec%phi(pest%vmec%ns) / (2*pi) * pest%vmec%isigng ! isigns is called signgs in the wout*.nc file. Why is this signgs here?

    ! Set reference length and magnetic field for GS2's normalization, 
    ! using the choices made by Pavlos Xanthopoulos in GIST:
    pest%major_R = pest%vmec%Rmajor
    pest%minor_r = pest%vmec%Aminor ! Note that 'Aminor' in read_wout_mod is called 'Aminor_p' in the wout*.nc file.

    if (norm_type == 'minor_r') then
      pest%L_ref = pest%minor_r
    else
      pest%L_ref = pest%major_R
    end if

    pest%B_ref = 2 * abs(edge_toroidal_flux_over_2pi) / (pest%L_ref * pest%L_ref)
    if (verbose) then
       print *,"  Reference length for normalization:",pest%L_ref," meters."
       print *,"  Reference magnetic field strength normalization:",pest%B_ref," Tesla."
    end if
  end subroutine

  subroutine destroy_PEST_Obj(pest)
    type(PEST_Obj), intent(inout) :: pest
    
    call destroy_VMEC_Obj(pest%vmec)
    deallocate(pest%iota)
    deallocate(pest%s_hat)
    deallocate(pest%safety_factor_q)
    deallocate(pest%psi)
    deallocate(pest%alpha)
    deallocate(pest%zeta)
    deallocate(pest%grad_psi_X)
    deallocate(pest%grad_psi_Y)
    deallocate(pest%grad_psi_Z)
    deallocate(pest%grad_alpha_X)
    deallocate(pest%grad_alpha_Y)
    deallocate(pest%grad_alpha_Z)
    deallocate(pest%grad_zeta_X)
    deallocate(pest%grad_zeta_Y)
    deallocate(pest%grad_zeta_Z)
    deallocate(pest%grad_theta_X)
    deallocate(pest%grad_theta_Y)
    deallocate(pest%grad_theta_Z)
    deallocate(pest%basis_psi_X)
    deallocate(pest%basis_psi_Y)
    deallocate(pest%basis_psi_Z)
    deallocate(pest%basis_alpha_X)
    deallocate(pest%basis_alpha_Y)
    deallocate(pest%basis_alpha_Z)
    deallocate(pest%basis_zeta_X)
    deallocate(pest%basis_zeta_Y)
    deallocate(pest%basis_zeta_Z)
    deallocate(pest%basis_theta_X)
    deallocate(pest%basis_theta_Y)
    deallocate(pest%basis_theta_Z)
    deallocate(pest%B_X)
    deallocate(pest%B_Y)
    deallocate(pest%B_Z)
    deallocate(pest%grad_B_X)
    deallocate(pest%grad_B_Y)
    deallocate(pest%grad_B_Z)
    deallocate(pest%bmag)
    deallocate(pest%jacobian)
    deallocate(pest%g_psi_psi)
    deallocate(pest%g_psi_alpha)
    deallocate(pest%g_alpha_alpha)
    deallocate(pest%g_psi_zeta)
    deallocate(pest%g_alpha_zeta)
    deallocate(pest%g_zeta_zeta)
    deallocate(pest%grad_B_drift_psi)
    deallocate(pest%grad_B_drift_alpha)
    deallocate(pest%normal_curv)
    deallocate(pest%geodesic_curv)
    deallocate(pest%d_B_d_zeta)
    deallocate(pest%Rsurf)
    deallocate(pest%Zsurf)
    deallocate(pest%theta_pest)
    deallocate(pest%d_Lambda_d_theta_vmec)
  end subroutine

  subroutine get_PEST_surface_data(pest,idx1,data_name,surf_data)
    type(PEST_Obj), intent(in) :: pest
    integer, intent(in) :: idx1
    character(*), intent(in) :: data_name
    real(dp), dimension(pest%idx_zeta_1:pest%idx_zeta_2,pest%idx_alpha_1:pest%idx_alpha_2), intent(out) :: surf_data
    integer :: str_len

    select case(trim(data_name))
      case('grad_psi_X')
        surf_data = pest%grad_psi_X(:,:,idx1)
      case('grad_psi_Y')
        surf_data = pest%grad_psi_Y(:,:,idx1)
      case('grad_psi_Z')
        surf_data = pest%grad_psi_Z(:,:,idx1)
      case('grad_alpha_X')
        surf_data = pest%grad_alpha_X(:,:,idx1)
      case('grad_alpha_Y')
        surf_data = pest%grad_alpha_Y(:,:,idx1)
      case('grad_alpha_Z')
        surf_data = pest%grad_alpha_Z(:,:,idx1)
      case('grad_zeta_X')
        surf_data = pest%grad_zeta_X(:,:,idx1)
      case('grad_zeta_Y')
        surf_data = pest%grad_zeta_Y(:,:,idx1)
      case('grad_zeta_Z')
        surf_data = pest%grad_zeta_Z(:,:,idx1)
      case('grad_theta_X')
        surf_data = pest%grad_theta_X(:,:,idx1)
      case('grad_theta_Y')
        surf_data = pest%grad_theta_Y(:,:,idx1)
      case('grad_theta_Z')
        surf_data = pest%grad_theta_Z(:,:,idx1)
      case('basis_psi_X')
        surf_data = pest%basis_psi_X(:,:,idx1)
      case('basis_psi_Y')
        surf_data = pest%basis_psi_Y(:,:,idx1)
      case('basis_psi_Z')
        surf_data = pest%basis_psi_Z(:,:,idx1)
      case('basis_alpha_X')
        surf_data = pest%basis_alpha_X(:,:,idx1)
      case('basis_alpha_Y')
        surf_data = pest%basis_alpha_Y(:,:,idx1)
      case('basis_alpha_Z')
        surf_data = pest%basis_alpha_Z(:,:,idx1)
      case('basis_zeta_X')
        surf_data = pest%basis_zeta_X(:,:,idx1)
      case('basis_zeta_Y')
        surf_data = pest%basis_zeta_Y(:,:,idx1)
      case('basis_zeta_Z')
        surf_data = pest%basis_zeta_Z(:,:,idx1)
      case('basis_theta_X')
        surf_data = pest%basis_theta_X(:,:,idx1)
      case('basis_theta_Y')
        surf_data = pest%basis_theta_Y(:,:,idx1)
      case('basis_theta_Z')
        surf_data = pest%basis_theta_Z(:,:,idx1)
      case('B_X')
        surf_data = pest%B_X(:,:,idx1)
      case('B_Y')
        surf_data = pest%B_Y(:,:,idx1)
      case('B_Z')
        surf_data = pest%B_Z(:,:,idx1)
      case('grad_B_X')
        surf_data = pest%grad_B_X(:,:,idx1)
      case('grad_B_Y')
        surf_data = pest%grad_B_Y(:,:,idx1)
      case('grad_B_Z')
        surf_data = pest%grad_B_Z(:,:,idx1)
      case('g_psi_psi')
        surf_data = pest%g_psi_psi(:,:,idx1)
      case('g_psi_alpha')
        surf_data = pest%g_psi_alpha(:,:,idx1)
      case('g_alpha_alpha')
        surf_data = pest%g_alpha_alpha(:,:,idx1)
      case('g_psi_zeta')
        surf_data = pest%g_psi_zeta(:,:,idx1)
      case('g_alpha_zeta')
        surf_data = pest%g_alpha_zeta(:,:,idx1)
      case('g_zeta_zeta')
        surf_data = pest%g_zeta_zeta(:,:,idx1)
      case('bmag')
        surf_data = pest%bmag(:,:,idx1)
      case('jacobian')
        surf_data = pest%jacobian(:,:,idx1)
      case('normal_curv')
        surf_data = pest%normal_curv(:,:,idx1)
      case('geodesic_curv')
        surf_data = pest%geodesic_curv(:,:,idx1)
      case('d_B_d_zeta')
        surf_data = pest%d_B_d_zeta(:,:,idx1)
      case('R')
        surf_data = pest%Rsurf(:,:,idx1)
      case('Z')
        surf_data = pest%Zsurf(:,:,idx1)
      case('theta')
        surf_data = pest%theta_pest(:,:,idx1)
      case default
        write(6,"(A)") "VMEC2PEST error! Data field with name ",trim(data_name)," does not exist!"
        stop
    end select
  end subroutine 

  subroutine get_PEST_volume_data(pest,data_name,vol_data)
    type(PEST_Obj), intent(in) :: pest
    character(*), intent(in) :: data_name
    real(dp), dimension(pest%idx_zeta_1:pest%idx_zeta_2,pest%idx_alpha_1:pest%idx_alpha_2,pest%idx_psi_1:pest%idx_psi_2), intent(out) :: vol_data 

    select case(trim(data_name))
      case('grad_psi_X')
        vol_data = pest%grad_psi_X
      case('grad_psi_Y')
        vol_data = pest%grad_psi_Y
      case('grad_psi_Z')
        vol_data = pest%grad_psi_Z
      case('grad_alpha_X')
        vol_data = pest%grad_alpha_X
      case('grad_alpha_Y')
        vol_data = pest%grad_alpha_Y
      case('grad_alpha_Z')
        vol_data = pest%grad_alpha_Z
      case('grad_zeta_X')
        vol_data = pest%grad_zeta_X
      case('grad_zeta_Y')
        vol_data = pest%grad_zeta_Y
      case('grad_zeta_Z')
        vol_data = pest%grad_zeta_Z
      case('grad_theta_X')
        vol_data = pest%grad_theta_X
      case('grad_theta_Y')
        vol_data = pest%grad_theta_Y
      case('grad_theta_Z')
        vol_data = pest%grad_theta_Z
      case('basis_psi_X')
        vol_data = pest%basis_psi_X
      case('basis_psi_Y')
        vol_data = pest%basis_psi_Y
      case('basis_psi_Z')
        vol_data = pest%basis_psi_Z
      case('basis_alpha_X')
        vol_data = pest%basis_alpha_X
      case('basis_alpha_Y')
        vol_data = pest%basis_alpha_Y
      case('basis_alpha_Z')
        vol_data = pest%basis_alpha_Z
      case('basis_zeta_X')
        vol_data = pest%basis_zeta_X
      case('basis_zeta_Y')
        vol_data = pest%basis_zeta_Y
      case('basis_zeta_Z')
        vol_data = pest%basis_zeta_Z
      case('basis_theta_X')
        vol_data = pest%basis_theta_X
      case('basis_theta_Y')
        vol_data = pest%basis_theta_Y
      case('basis_theta_Z')
        vol_data = pest%basis_theta_Z
      case('B_X')
        vol_data = pest%B_X
      case('B_Y')
        vol_data = pest%B_Y
      case('B_Z')
        vol_data = pest%B_Z
      case('grad_B_X')
        vol_data = pest%grad_B_X
      case('grad_B_Y')
        vol_data = pest%grad_B_Y
      case('grad_B_Z')
        vol_data = pest%grad_B_Z
      case('g_psi_psi')
        vol_data = pest%g_psi_psi
      case('g_psi_alpha')
        vol_data = pest%g_psi_alpha
      case('g_alpha_alpha')
        vol_data = pest%g_alpha_alpha
      case('g_psi_zeta')
        vol_data = pest%g_psi_zeta
      case('g_alpha_zeta')
        vol_data = pest%g_alpha_zeta
      case('g_zeta_zeta')
        vol_data = pest%g_zeta_zeta
      case('bmag')
        vol_data = pest%bmag
      case('jacobian')
        vol_data = pest%jacobian
      case('normal_curv')
        vol_data = pest%normal_curv
      case('geodesic_curv')
        vol_data = pest%geodesic_curv
      case('d_B_d_zeta')
        vol_data = pest%d_B_d_zeta
      case default
        write(6,"(A)") "VMEC2PEST error! Data field with name ",trim(data_name)," does not exist!"
        stop
    end select
  end subroutine

  subroutine get_PEST_field_line_data(pest,idx1,idx2,data_name,line_data)
    type(PEST_Obj), intent(in) :: pest
    integer, intent(in) :: idx1, idx2
    character(*), intent(in) :: data_name
    real(dp), dimension(pest%idx_zeta_1:pest%idx_zeta_2), intent(out) :: line_data

    select case(trim(data_name))
      case('grad_psi_X')
        line_data = pest%grad_psi_X(:,idx2,idx1)
      case('grad_psi_Y')
        line_data = pest%grad_psi_Y(:,idx2,idx1)
      case('grad_psi_Z')
        line_data = pest%grad_psi_Z(:,idx2,idx1)
      case('grad_alpha_X')
        line_data = pest%grad_alpha_X(:,idx2,idx1)
      case('grad_alpha_Y')
        line_data = pest%grad_alpha_Y(:,idx2,idx1)
      case('grad_alpha_Z')
        line_data = pest%grad_alpha_Z(:,idx2,idx1)
      case('grad_zeta_X')
        line_data = pest%grad_zeta_X(:,idx2,idx1)
      case('grad_zeta_Y')
        line_data = pest%grad_zeta_Y(:,idx2,idx1)
      case('grad_zeta_Z')
        line_data = pest%grad_zeta_Z(:,idx2,idx1)
      case('grad_theta_X')
        line_data = pest%grad_theta_X(:,idx2,idx1)
      case('grad_theta_Y')
        line_data = pest%grad_theta_Y(:,idx2,idx1)
      case('grad_theta_Z')
        line_data = pest%grad_theta_Z(:,idx2,idx1)
      case('basis_psi_X')
        line_data = pest%basis_psi_X(:,idx2,idx1)
      case('basis_psi_Y')
        line_data = pest%basis_psi_Y(:,idx2,idx1)
      case('basis_psi_Z')
        line_data = pest%basis_psi_Z(:,idx2,idx1)
      case('basis_alpha_X')
        line_data = pest%basis_alpha_X(:,idx2,idx1)
      case('basis_alpha_Y')
        line_data = pest%basis_alpha_Y(:,idx2,idx1)
      case('basis_alpha_Z')
        line_data = pest%basis_alpha_Z(:,idx2,idx1)
      case('basis_zeta_X')
        line_data = pest%basis_zeta_X(:,idx2,idx1)
      case('basis_zeta_Y')
        line_data = pest%basis_zeta_Y(:,idx2,idx1)
      case('basis_zeta_Z')
        line_data = pest%basis_zeta_Z(:,idx2,idx1)
      case('B_X')
        line_data = pest%B_X(:,idx2,idx1)
      case('B_Y')
        line_data = pest%B_Y(:,idx2,idx1)
      case('B_Z')
        line_data = pest%B_Z(:,idx2,idx1)
      case('grad_B_X')
        line_data = pest%grad_B_X(:,idx2,idx1)
      case('grad_B_Y')
        line_data = pest%grad_B_Y(:,idx2,idx1)
      case('grad_B_Z')
        line_data = pest%grad_B_Z(:,idx2,idx1)
      case('g_psi_psi')
        line_data = pest%g_psi_psi(:,idx2,idx1)
      case('g_psi_alpha')
        line_data = pest%g_psi_alpha(:,idx2,idx1)
      case('g_alpha_alpha')
        line_data = pest%g_alpha_alpha(:,idx2,idx1)
      case('g_psi_zeta')
        line_data = pest%g_psi_zeta(:,idx2,idx1)
      case('g_alpha_zeta')
        line_data = pest%g_alpha_zeta(:,idx2,idx1)
      case('g_zeta_zeta')
        line_data = pest%g_zeta_zeta(:,idx2,idx1)
      case('bmag')
        line_data = pest%bmag(:,idx2,idx1)
      case('jacobian')
        line_data = pest%jacobian(:,idx2,idx1)
      case('normal_curv')
        line_data = pest%normal_curv(:,idx2,idx1)
      case('geodesic_curv')
        line_data = pest%geodesic_curv(:,idx2,idx1)
      case('d_B_d_zeta')
        line_data = pest%d_B_d_zeta(:,idx2,idx1)
      case('x3')
        line_data = pest%zeta(:,idx2,idx1)
      case default
        write(6,"(A)") "VMEC2PEST error! Data field with name ",trim(data_name)," does not exist!"
        stop
    end select
  end subroutine

  subroutine get_PEST_radial_data(pest,idx1,data_name,rad_data)
    type(PEST_Obj), intent(in) :: pest
    integer, intent(in) :: idx1
    character(*), intent(in) :: data_name
    real(dp), intent(out) :: rad_data

    select case(trim(data_name))
      case('iota')
        rad_data = pest%iota(idx1)
      case('safety_factor_q')
        rad_data = pest%safety_factor_q(idx1)
      case('s_hat')
        rad_data = pest%s_hat(idx1)
      case('x1')
        !rad_data = pest%psi(1,1,idx1)
        rad_data = pest%psi(0,0,idx1)
      case default
        write(6,"(A)") "VMEC2PEST error! Data field with name ",trim(data_name)," does not exist!"
        stop
    end select
  end subroutine

  subroutine get_PEST_VMEC_data(pest,data_name,vmec_data)
    type(PEST_Obj), intent(in) :: pest
    character(*), intent(in) :: data_name
    real(dp), intent(out) :: vmec_data

    select case(trim(data_name))
      case('nfp')
        vmec_data = pest%vmec%nfp
      case default
        write(6,"(A)") "VMEC2PEST error! Data field with name ",trim(data_name)," does not exist!"
        stop
    end select
  end subroutine
end module 
