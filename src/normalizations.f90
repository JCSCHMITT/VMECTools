!******************************************************************************
! This module defines normalizations for different simuation grid types
! that are computed through the  vmec2pest calculation
! Author: B.J. Faber (bfaber@wisc.edu)
! Creation date: June 2019
!******************************************************************************
module normalizations
  use types, only: dp, pi
  use pest_object, only: PEST_Obj
  implicit none

  public set_normalizations

contains
 
  subroutine set_normalizations(pest,grid_type)
    type(PEST_Obj), intent(inout) :: pest
    character(*), intent(in) :: grid_type
    select case(trim(grid_type))
      case('gene')
        call gene_normalizations(pest)
      case('gs2')
        call gs2_normalizations(pest)
      case default
        print *, "Nothing happening"
    end select
  end subroutine
 
  subroutine gene_normalizations(pest)
    type(PEST_Obj), intent(inout) :: pest
    integer :: idx1, idx2, idx3
    
    ! Adjust the jacobian
    do idx1 = pest%idx_psi_1,pest%idx_psi_2
      do idx2 = pest%idx_alpha_1,pest%idx_alpha_2
        do idx3 = pest%idx_zeta_1, pest%idx_zeta_2
          pest%jacobian(idx3,idx2,idx1) = abs(pest%jacobian(idx3,idx2,idx1)/(1.0+pest%d_Lambda_d_theta_vmec(idx3,idx2,idx1)) )
        end do
      end do
      ! GENE uses theta as the parallel coordinate 
      pest%g_psi_psi(:,:,idx1) = pest%L_ref * pest%L_ref/(4.0*pest%psi(pest%idx_zeta_1,pest%idx_alpha_1,idx1))*pest%g_psi_psi(:,:,idx1) 
      pest%g_psi_alpha(:,:,idx1) = 0.5*pest%L_ref*pest%L_ref*pest%g_psi_alpha(:,:,idx1)
      !pest%g_alpha_alpha(:,:,idx1) = pest%psi(1,1,pest%idx_psi_1) * pest%L_ref * pest%L_ref * pest%g_alpha_alpha(:,:,idx1)
      pest%g_alpha_alpha(:,:,idx1) = pest%psi(0,0,pest%idx_psi_1) * pest%L_ref * pest%L_ref * pest%g_alpha_alpha(:,:,idx1)
      pest%g_psi_zeta(:,:,idx1) = 0.5*pest%L_ref*pest%L_ref/sqrt(pest%psi(pest%idx_zeta_1,pest%idx_alpha_1,idx1))*pest%g_psi_zeta(:,:,idx1)
      pest%g_alpha_zeta(:,:,idx1) = pest%L_ref*pest%L_ref*sqrt(pest%psi(pest%idx_zeta_1,pest%idx_alpha_1,idx1))*pest%g_alpha_zeta(:,:,idx1)
      pest%g_zeta_zeta(:,:,idx1) = pest%L_ref*pest%L_ref*pest%g_zeta_zeta(:,:,idx1)
      pest%grad_B_drift_psi(:,:,idx1) = pest%L_ref*pest%L_ref*sqrt(pest%psi(pest%idx_zeta_1,pest%idx_alpha_1,idx1))*pest%grad_B_drift_psi(:,:,idx1)/pest%bmag(:,:,idx1)
      pest%grad_B_drift_alpha(:,:,idx1) = pest%L_ref*pest%L_ref/(2.0*sqrt(pest%psi(pest%idx_zeta_1,pest%idx_alpha_1,idx1)))*pest%grad_B_drift_alpha(:,:,idx1)/pest%bmag(:,:,idx1)
      pest%d_B_d_zeta(:,:,idx1) = pest%L_ref*pest%d_B_d_zeta(:,:,idx1)/(pest%vmec%nfp*pest%jacobian(:,:,idx1)*pest%bmag(:,:,idx1))
      pest%bmag(:,:,idx1) = 1.0/pest%B_ref*pest%bmag(:,:,idx1)
      pest%jacobian(:,:,idx1) = 2.0*pest%safety_factor_q(idx1)/(pest%L_ref*pest%L_ref*pest%L_ref)*pest%jacobian(:,:,idx1)
    end do
  end subroutine

  subroutine gs2_normalizations(pest)
    type(PEST_Obj), intent(inout) :: pest

    pest%bmag = 1.0/pest%B_ref*pest%bmag

  end subroutine

end module
